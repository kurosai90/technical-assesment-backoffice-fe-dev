import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initLoginForm();
  }

  submitLogin(){
    console.log(this.loginForm.value);
    const loginValid = this.loginForm.value.username === 'Fikri1990' && this.loginForm.value.password === 'Fikri123.';
    if (loginValid){
      this.router.navigate([
        'employees/list'
      ]);
    }else{
      alert('Wrong credentials!');
    }
  }

  initLoginForm(){
    this.loginForm = this.fb.group({
      username: [],
      password: []
    });
  }
}
