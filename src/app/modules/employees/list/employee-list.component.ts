import { Employee } from './../../../data/employee';
import { employeeData } from './../../../data/mock-data/employee-data';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  public employees: Employee[] = employeeData;
  public collectionSize = 100;
  public page = 1;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  navigateToAddEmployee(){
    this.router.navigate([
      '/employees/add/'
    ]);
  }

}
