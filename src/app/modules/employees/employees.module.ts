import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDatepickerModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeeListComponent } from './list/employee-list.component';
import { AddEmployeeComponent } from './add/add-employee.component';


@NgModule({
  declarations: [EmployeeListComponent, AddEmployeeComponent],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    NgbPaginationModule,
    NgbDatepickerModule
  ]
})
export class EmployeesModule { }
