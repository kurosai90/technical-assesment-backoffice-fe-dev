export const employeeData = [
    {
        username: 'Fariz123',
        firstName: 'Fariz',
        lastName: 'Zulfikar',
        email: 'fariz.zulfikar@gmail.com',
        birthDate: 123,
        basicSalary: 9000000,
        status: 'Staffs',
        group: 'IT-Developer',
        description: 'Programmer'
    },
    {
        username: 'Rouza97',
        firstName: 'Rouzaliyana',
        lastName: 'Aushuria',
        email: 'rouzaliyana.aushuria@gmail.com',
        birthDate: 54321,
        basicSalary: 10000000,
        status: 'Supervisor',
        group: 'IT-PMO',
        description: 'Project Manager Officer'
    },
    {
        username: 'Rega123',
        firstName: 'Mochammad',
        lastName: 'Rega',
        email: 'moch.rega@gmail.com',
        birthDate: 555,
        basicSalary: 8500000,
        status: 'Staff',
        group: 'UI/UX Designer',
        description: 'Designer'
    }
];
