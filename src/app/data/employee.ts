export class Employee{
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: number;
    basicSalary: number;
    status: string;
    group: string;
    description: string;
}
